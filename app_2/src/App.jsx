import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import MainLayout from "./layout/MainLayout";

ReactDOM.render(<MainLayout />, document.getElementById("app"));
